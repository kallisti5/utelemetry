use std::error::Error;
use std::sync::Mutex;
use std::collections::HashMap;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct CrashReport {
    pub source: String,
    pub version: String,
	pub detail: String
}

pub struct CrashDB {
	pub reports: Mutex<HashMap<usize, CrashReport>>
}

impl CrashDB {
    pub fn new() -> CrashDB {
        CrashDB {
            reports: Mutex::new(HashMap::<usize, CrashReport>::new())
        }
    }
    pub fn insert(&self, crash: CrashReport) -> Result<usize, Box<Error>> {
        // TODO: Make random
        let mut random_id: usize = 100;
        let mut reports = self.reports.lock().unwrap();
        while reports.contains_key(&random_id) {
            random_id += 1;
        }
        reports.insert(random_id, crash);
        return Ok(random_id);
    }

    pub fn locate(&self, id: usize) -> Result<CrashReport, Box<Error>> {
        let reports = self.reports.lock().unwrap();
        for key in reports.keys() {
            if key == &id {
                return Ok(reports[key].clone());
            }
        }
        return Err(From::from("No records found"));
    }
}

#[cfg(test)]
mod tests {
    use CrashDB;
    use CrashReport;

    #[test]
    fn new_crash_db() {
        let _db = CrashDB::new();
    }

    #[test]
    fn store_and_load_crashes() {
        let test_report = CrashReport {
            source: "user".to_string(),
            version: "1.0".to_string(),
            detail: "Printer is on fire!".to_string(),
        };

        // Grab Database
        let db = CrashDB::new();

        // Insert report, record id.
        let id = match db.insert(test_report.clone()) {
            Ok(id) => id,
            Err(x) => panic!(format!("Unable to insert: {}", x)),
        };

        // Locate report
        let db_result = db.locate(id).unwrap();

        // Validate same json
        assert_eq!(json!(db_result), json!(test_report));
    }
}
