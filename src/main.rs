#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use] extern crate rocket;
#[macro_use] extern crate rocket_contrib;
#[macro_use] extern crate serde_derive;

mod crashdb;

use rocket::State;
use rocket_contrib::json::{Json, JsonValue};

use crashdb::{CrashDB,CrashReport};

#[get("/")]
fn index() -> &'static str {
    "Haiku Telemetry\nDon't be freaked out, here we collect crash reports and other *anonymous* information from running Haiku systems."
}

#[put("/crash", format = "json", data = "<message>")]
fn put_crash(message: Json<CrashReport>, db: State<CrashDB>) -> JsonValue {
    println!("Received: {:?}", message);
    let result = match db.insert(message.clone()) {
        Ok(id) => json!({"status": "ok", "reason": format!("Report {} was accepted!", id)}),
        Err(e) => json!({"status": "error", "reason": format!("Error: {}", e)})
    };
    return result;
}

#[get("/crash", format = "json")]
fn get_crashes(db: State<CrashDB>) -> JsonValue {
    //let result = match db.reports.lock() {
    //    Ok(x) => json!(x.unwrap()),
    //    Err(e) => {
    //        return json!({"status": "error", "reason": format!("Unable to obtain lock: {}", e)})
    //    }
    //};
    return json!({"status": "error", "reason": "Not implemented yet!"});
}

#[get("/crash/<id>", format = "json")]
fn get_crash(id: usize, db: State<CrashDB>) -> JsonValue {
    let report = match db.locate(id) {
        Ok(r) => r,
        Err(e) => {
            return json!({"status": "error", "reason": format!("unable to find {}: {}", id, e)})
        },
    };
    json!({"status": "ok", "result": json!(report)})
}

#[catch(404)]
fn not_found() -> JsonValue {
    json!({
        "status": "error",
        "reason": "Resource was not found."
    })
}

fn rocket() -> rocket::Rocket {
    rocket::ignite()
        .mount("/", routes![index, get_crashes, get_crash, put_crash])
        .register(catchers![not_found])
        .manage(CrashDB::new())
}

fn main() {
    rocket().launch();
}
