# uTelemetry

A simple microservice to collect telemetry data from applications or operating systems

https://gitlab.com/kallisti5/utelemetry

## Testing

### Inserting Data

  * ```curl -H "Content-Type: application/json" -X PUT http://localhost:8000/crash --data '{"source": "debugger", "version": "r1beta1", "detail": "Oops"}'```

### Examining Data

  * ```curl -H "Content-Type: application/json" http://localhost:8000/crash```
  * ```curl -H "Content-Type: application/json" http://localhost:8000/crash/123```

## API Documentation

### PUT /crash

```json
{
	"source": "debugger",
	"version": "NeatSoftware, 1.0",
	"detail": "Backtrace doing stuff",
}
```

## License

Copyright, 2018 Alexander von Gluck IV, All rights reserved.
Released under the terms of the MIT License
